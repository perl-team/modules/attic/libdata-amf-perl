libdata-amf-perl (0.09+dfsg-3) UNRELEASED; urgency=medium

  [ Laurent Baillet ]
  * fix lintian wrong-path-for-interpreter error

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Add missing build dependency on libmodule-install-perl.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 25 Feb 2018 15:09:27 +0100

libdata-amf-perl (0.09+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove TANIGUCHI Takaki from Uploaders. Thanks for your work!

  [ Niko Tyni ]
  * Fix autopkgtest failures due to Any::Moose deprecation (See #845812)
  * Upgrade to debhelper compat level 9.
  * Update to Standards-Version 3.9.8
  * Remove most of inc/ apart from Module::Install.
    + implies a build dependency on libtest-base-perl

 -- Niko Tyni <ntyni@debian.org>  Sun, 04 Dec 2016 21:24:48 +0200

libdata-amf-perl (0.09+dfsg-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/copyright: update wording of Comment about copyright
    ownership.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Paul Wise ]
  * Strip Flash files since one is not buildable on Debian and
    the other is buildable but the result is untested (Closes: #736799)

 -- Paul Wise <pabs@debian.org>  Fri, 02 May 2014 11:49:04 +0800

libdata-amf-perl (0.09-3) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/rules: move the old included inc/YAML.pm away before running
    the test suite. Thanks to Dominic Hargreaves for the bugreport
    (closes: #646289).

 -- gregor herrmann <gregoa@debian.org>  Sun, 23 Oct 2011 01:08:35 +0200

libdata-amf-perl (0.09-2) unstable; urgency=low

  * Add build dependency on libyaml-perl (closes: #627170).
  * Switch to source format 3.0 (quilt).
  * debian/copyright: add missing pieces.
  * Bump debhelper compatibility level to 8.
  * debian/control: remove version from perl build dependency (satisfied in
    oldstable), remove perl from Depends (added by ${perl:Depends}).
  * Set Standards-Version to 3.9.2 (no changes).
  * Add /me to Uploaders.
  * debian/rules: fix permissions of example script; don't install manpage
    fragments.

 -- gregor herrmann <gregoa@debian.org>  Wed, 18 May 2011 23:14:52 +0200

libdata-amf-perl (0.09-1) unstable; urgency=low

  * Initial Release. (Closes: #620956)

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 07 Apr 2011 10:58:39 +0900
